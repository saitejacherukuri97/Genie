import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './auth-app/components/signin/signin.component';
import { AuthGuard } from './auth-app/guards/authentication.guard';
import { HomeComponent } from './components/home/home.component';
import { DashboardComponent } from './dashboard/components/dashboard/dashboard.component';
import { ProjectsHomeComponent } from './project-management/components/projects-home/projects-home.component';
import { LeaveHomeComponent } from './leave-management/components/leave-home/leave-home.component';
import { BroadcastHomeComponent } from './broadcast-management/components/broadcast-home/broadcast-home.component';
import { BillingMontoringComponent } from './billing-management/components/billing-montoring/billing-montoring.component';


const homeChildRoutes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'project-management', component: ProjectsHomeComponent},
  { path: 'billing-management', component: BillingMontoringComponent},
  { path: 'leave-management', component: LeaveHomeComponent},
  { path: 'configuration', component: DashboardComponent},
  { path: 'broadcast-management', component: BroadcastHomeComponent},
  { path: 'billing-management/monitoring', component: BillingMontoringComponent},
]

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },
  { path: 'login', component: SigninComponent },
  { path: 'home', component: HomeComponent, children: homeChildRoutes, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
