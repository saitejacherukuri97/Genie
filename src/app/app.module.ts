import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { AuthAppModule } from './auth-app/auth-app.module';
import { HomeComponent } from './components/home/home.component';
import { FakeAuthenticationService } from './auth-app/services/fake-backend.service';
import { AngularMatModule } from './angular-mat/angular-mat.module';
import { ProjectManagementModule } from './project-management/project-management.module';
import { ConfigurationModule } from './configuration/configuration.module';
import { BillingManagementModule } from './billing-management/billing-management.module';
import { LeaveManagementModule } from './leave-management/leave-management.module';
import { BroadcastManagementModule } from './broadcast-management/broadcast-management.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { BillingMontoringComponent } from './billing-management/components/billing-montoring/billing-montoring.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    BillingMontoringComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMatModule,
    AuthAppModule,
    DashboardModule,
    BillingManagementModule,
    ProjectManagementModule,
    ConfigurationModule,
    LeaveManagementModule,
    BroadcastManagementModule,
  ],
  providers: [FakeAuthenticationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
