import { NgModule } from '@angular/core';
import { SigninComponent } from './components/signin/signin.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { AngularMatModule } from '../angular-mat/angular-mat.module';




@NgModule({
  declarations: [SigninComponent],
  imports: [
    HttpClientModule,
    BrowserModule,
    AngularMatModule
  ]
})
export class AuthAppModule { }
