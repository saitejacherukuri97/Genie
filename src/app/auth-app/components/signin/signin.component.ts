import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../../models/user';
import { AuthenticationService } from '../../services/authentication.service';
import {RouterModule, Routes, Router } from '@angular/router'
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css',
              '../../../../assets/css/login_page.css',]
})
export class SigninComponent implements OnInit {

  private user : User;
  private homeUrl : string = '/home/dashboard';
  private loginUrl : string = '/login';
  private error: string;
  private success: string;
  constructor(private authenticationService: AuthenticationService, private routers: Router) { }

  ngOnInit() {
  }

  signin(loginForm : NgForm): void{
    console.log('signing in...')
    if(loginForm.valid){
        this.user = new User();
        this.user['username'] = loginForm.value.username;
        this.user['password'] = loginForm.value.password;
        this.authenticationService.login(this.user)
            .pipe(first())
            .subscribe(
              data => {
                  this.success = 'Login Successful' ;
                  this.routers.navigate([this.homeUrl]);
              },
             error => {
                  this.error = 'Invalid Id/Password'
             });
    }
  }

  // notifyClose(message:string): void {
  //   console.log("hi");
  //   if(document.getElementById("notify-message").onchange){
  //     setTimeout(() => {
  //       document.getElementById("notify-message").click();
  //   }, 2000);
  //   }   
  // }
    

}
