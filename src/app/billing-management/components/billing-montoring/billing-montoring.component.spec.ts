import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillingMontoringComponent } from './billing-montoring.component';

describe('BillingMontoringComponent', () => {
  let component: BillingMontoringComponent;
  let fixture: ComponentFixture<BillingMontoringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillingMontoringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillingMontoringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
