import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-billing-montoring',
  templateUrl: './billing-montoring.component.html',
  styleUrls: ['./billing-montoring.component.css']
})
export class BillingMontoringComponent implements OnInit {

  private cards : any [];
  private clickedCard : number ;
  constructor() { }
  ngOnInit() {
    this.cards = [
      {"teamName" : "RMS", "teamLead": "Tulasi Viraja"},
      {"teamName" : "MXINT", "teamLead": "Srivatsan"},
      {"teamName" : "Genie", "teamLead": "Tapan Jain"},
      {"teamName" : "DigiBank", "teamLead": "Russel Peter"},
      {"teamName" : "Murex Core", "teamLead": "Lee Hu Chang"},
      {"teamName" : "Murex side", "teamLead": "Kung Wang"},
      {"teamName" : "iWork", "teamLead": "Andrew Ng"},
      {"teamName" : "DigiBank", "teamLead": "Russel Peter"},
      {"teamName" : "Murex Core", "teamLead": "Lee Hu Chang"},
      {"teamName" : "Murex side", "teamLead": "Kung Wang"},   
    ];
  }

  onCardClick(card: any){

    this.clickedCard   = card.teamName;
  }
}
