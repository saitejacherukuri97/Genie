import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BroadcastHomeComponent } from './components/broadcast-home/broadcast-home.component';



@NgModule({
  declarations: [BroadcastHomeComponent],
  imports: [
    CommonModule
  ]
})
export class BroadcastManagementModule { }
