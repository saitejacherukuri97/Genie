import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BroadcastHomeComponent } from './broadcast-home.component';

describe('BroadcastHomeComponent', () => {
  let component: BroadcastHomeComponent;
  let fixture: ComponentFixture<BroadcastHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BroadcastHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BroadcastHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
