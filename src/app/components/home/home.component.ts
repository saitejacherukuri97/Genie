import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/auth-app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private tipPosition = 'below';
  private assignmentBadge: number = 10;
  private navBarLinks: any = [];
  private navBarDropDowns: any = [];
  private routeLinks: any;
  constructor(
    private authenticationService: AuthenticationService,
    private router: Router) {
    this.navBarLinks = [
      {
        "label": "Project Management",
        "link": "project-management"
      },
      {
        "label": "Billing",
        "link": "billing-management"
      },
      {
        "label": "Configuration",
        "link": "configuration"
      },
      {
        "label": "Broadcast",
        "link": "broadcast-management"
      }
    ];

    this.navBarDropDowns = [
      {
        "label": "Worklog",
        "menu": [
          {
            "label": 'TimeSheets',
            "link": 'Timesheet'
          },
          {
            "label": 'Monitor',
            "link": 'billing-management/monitoring'
          }
        ]
      }
    ]
  }

  ngOnInit() {
  }

  logout(): void {
    this.authenticationService.logout();
    this.router.navigate(['login']);
  }

  routeTo(module: string) {
    this.router.navigate([this.routeLinks[module]]);
  }

  getUrl(): string{
    let url: string = this.router.url;
    let vals: string [] = url.split('/');
    let urlString : string = "";
    vals.forEach(
      (ele)=> {
        if(ele){
          ele = ele.replace('-', ' ');
          urlString = urlString.concat(ele + " / ");
        }
      }
    );
    return urlString;
  }


}
