import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  private name :string = 'Deepak';
  private currentDate:Date = new Date();
  private profilLink :string = '/profile';
  constructor() { }

  ngOnInit() {
  }

}
