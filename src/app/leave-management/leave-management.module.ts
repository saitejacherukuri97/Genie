import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeaveHomeComponent } from './components/leave-home/leave-home.component';
import { TimeSheetComponent } from './components/time-sheet/time-sheet.component';



@NgModule({
  declarations: [LeaveHomeComponent, TimeSheetComponent],
  imports: [
    CommonModule
  ]
})
export class LeaveManagementModule { }
