import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectsHomeComponent } from './components/projects-home/projects-home.component';



@NgModule({
  declarations: [ProjectsHomeComponent],
  imports: [
    CommonModule
  ]
})
export class ProjectManagementModule { }
