import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/auth-app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  private assingmentBadge: number = 5;

  private navLinkItems = ['Project Management', 'Billing', 'Leave Managment', 'Configuration', 'Broadcast'];
  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  private routeLinks: any = {
    "Project Management": "/project-management",
    "Billing": "billing",
    "Leave Managment": "/leave-management",
    "Configuration": "/configuration",
    "Broadcast": "/broadcast-management",
    "Dashboard" : "dashboard"
  }

  ngOnInit() {
  }

  logout(): void {
    this.authenticationService.logout();
    this.router.navigate(['login']);
  }

  routeTo(module: string) {
    this.router.navigate([this.routeLinks[module]]);
  }


}
